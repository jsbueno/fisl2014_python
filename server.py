# coding: utf-8
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import sqlite3
import os


TEMPLATE = """
<html>
<body>
<h1>Eventos do tipo %s</h1>
    <ol>
    %s
    </ol>
</body>
</html>
"""

connection = sqlite3.connect("banklog.db")

class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        global connection
        cursor = connection.cursor()
        path_comp = self.path.strip("/").split("/")[0]
        if path_comp not in ("Installed", "Updated", "Deleted", "Detail"):
            self.send_response(404)
            self.send_header("Content-type", "text/plain; charset=UTF-8")
            self.end_headers()
            self.wfile.write("404 - Página não encontrada")
            return
        self.send_response(200)
        self.send_header("Content-type", "text/html; charset=UTF-8")
        self.end_headers()
        if path_comp != "Detail":
            data = cursor.execute("SELECT timestamp, data, data FROM log WHERE event=?", (path_comp,)).fetchall()
            formatted_data = [
                """\t<li>%s - <a href="/Detail/%s">%s</a>""" %
                line for line in data]
        else:
            data = os.popen("rpm -ql %s" % self.path.split("/")[-1]).readlines()
            formatted_data = ["\t<li>%s</li>" % line for line in data]
        self.wfile.write(TEMPLATE % (path_comp, "\n".join(formatted_data) ))

server  = HTTPServer(("127.0.0.1", 8000), Handler)
server.serve_forever()