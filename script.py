import os
import sqlite3
from datetime import datetime

def setup():
    os.unlink("banklog.db")
    con = sqlite3.connect("banklog.db")
    cursor = con.cursor()
    cursor.execute("""CREATE TABLE log 
                        (timestamp DATETIME,
                        event VARCHAR(255),
                        data VARCHAR(255))
                    """)
    return con

def importdata(con, filename="yum.log"):
    cursor = con.cursor()
    #year = str(datetime.fromtimestamp(os.stat("yum.log").st_mtime).year) + " "
    year = "2014 "

    for line in open(filename):
        if not line: 
            pass
        timetext, event, data = line.rsplit(" ", 2)
        timestamp = datetime.strptime(year + timetext,  "%Y %b %d %H:%M:%S")
        event = event.strip(":")
        data = data.strip()
        cursor.execute("INSERT INTO log VALUES(?, ?, ?)", (timestamp, event, data))
    con.commit()

def show_results(con):
    from pprint import pprint
    pprint(con.execute("SELECT * from log where timestamp > '2014-04-15'").fetchall())

con = setup()
importdata(con)
show_results(con)